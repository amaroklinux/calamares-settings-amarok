# Calamares Settings Amarok
Amarok Linux theme and settings for the Calamares Installer

 Calamares is a generic installer framework for Linux distributions.
 By default, it contains a set of boilerplate wording and images. This
 package provides the latest Debian artwork as well as scripts that
 supports EFI installations.
