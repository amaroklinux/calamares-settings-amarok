import QtQuick 2.0;
import calamares.slideshow 1.0;

Presentation
{
    id: presentation

    Timer {
        interval: 10000
        running: true
        repeat: true
        onTriggered: presentation.goToNextSlide()
    }
     Slide {
        Image {
            anchors.centerIn: parent
            id: image1
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "welcome.png"
        }
    }
     Slide {
        Image {
            anchors.centerIn: parent
            id: image2
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "acessorios-amarok.png"
        }
    }
     Slide {
        Image {
            anchors.centerIn: parent
            id: image3
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "games-amarok.png"
        }
    }
     Slide {
        Image {
            anchors.centerIn: parent
            id: image4
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "firefox-amarok.png"
        }
    }
    Slide {
        Image {
            anchors.centerIn: parent
            id: image5
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "midia-amarok.png"
        }
    }
     Slide {
        Image {
            anchors.centerIn: parent
            id: image6
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "office-amarok.png"
        }
    }
    Slide {
        Image {
            anchors.centerIn: parent
            id: image7
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "gimp-amarok.png"
        }
    }
    Slide {
        Image {
            anchors.centerIn: parent
            id: image8
            x:0
            y:0
            width: 700
            height: 450
            fillMode: Image.PreserveAspectFit
            smooth: true
            source: "FindUs.png"
        }
    }
}
